<?php
include_once 'includes/user.php';
include_once 'includes/user_session.php';

$userSession = new UserSession();
$user = new User();

if(isset($_SESSION['user'])){
    //echo "hay sesion";
    $user->setUser($userSession->getCurrentUser());
    if($user->getRol() == 1){
        include_once 'admin/home.php';
    }else{
        include_once 'cliente/home.php';
    }

}

if(isset($_POST['username']) && isset($_POST['password'])){
    
    $userForm = $_POST['username'];
    $passForm = $_POST['password'];
   
    $user = new User();
    if($user->userExists($userForm, $passForm)){
        //echo "Existe el usuario";
        $userSession->setCurrentUser($userForm);
        $user->setUser($userForm);
        if($user->getRol() == 1){
            include_once 'admin/home.php';
        }else{
            include_once 'cliente/home.php';
        }
        
    }else{
        //echo "No existe el usuario";
        if(isset($_GET["page"])){

            include_once $_GET["page"];
        }else{
            include_once 'vistas/login.php';
        }
    }

}else{
    if(isset($_GET["page"])){
        include_once $_GET["page"];
    }else{
        include_once 'vistas/login.php';
    }
}



?>