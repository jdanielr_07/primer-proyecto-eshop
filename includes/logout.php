<?php
    include_once 'delete.php';
    include_once 'user_session.php';
    /**
     * Cierra la sesión.
     */
    $userSession = new UserSession();
    $userSession->closeSession();

    header("location: ../index.php");

?>