<?php
  $sql = 'SELECT * FROM categorias';
  $connection = new mysqli('localhost', 'root', 'Logaritmos506', 'primer_proyecto_web1');
  $result = $connection->query($sql);
  $categorias = $result->fetch_all();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
#g-table tbody tr > td{
    border: 1px solid rgb(220,220,220);
    height: 30px;
    padding-left: 3px;
}
#g-table{
    padding-left: 40px;
    margin-top: 20px;

}
nav > ul {
  display: flex;
  flex-direction: column;
  align-items: center;
}

</style>
<body style="background-color:gray">
    <div id="menu">
        <ul>
            <li>Home - Administrador</li>
            <li class="cerrar-sesion"><a href="includes/logout.php">Cerrar sesión</a></li>
        </ul>
    </div>
    <section>
        <h1 style="color:white;">Bienvenido <?php echo $user->getNombre();  ?></h1>  
    </section>
    <table align="center" class="table table-light"  id="g-table">
      <tr>
        <th>ID </th>
        <th> Categoría</th>
      </tr>
      <tbody>
        <?php 
          $page = 'admin/admin_categorias.php'; 
            foreach ($categorias as $categoria) {
                echo "<tr><td>".$categoria[0]."</td><td>".$categoria[1]."</td><td><a href='index_admin.php?page=admin/edit_categoria.php&id=".$categoria[0]."'><input type='button' value='Editar 📝'></a></td><td><a href='delete.php?id=".$categoria[0]."&table=categorias&field=id&page=index_admin.php?page=".$page."'><input type='button' value='Eliminar 🗑'></a></td></tr>";  
            }             
        ?>
      </tbody>
    </table>
    <div style="text-align: center;">
      <a href="index.php"><input type="button" value="Home - Admin"></a>
      <a href="index_admin.php?page=admin/admin_nueva_categoria.php"><input type="button" value="Crear nueva categoría"></a>
    </div>
</body>
</html>