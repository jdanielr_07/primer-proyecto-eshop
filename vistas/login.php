<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="main_login.css">
</head>
<body>
    <form action="" method="POST">
        <?php
            if(isset($errorLogin)){
                echo $errorLogin;
            }
        ?>
        <div class="body"></div>
		<div class="grad"></div>
		<div class="header">
			<div>EShop<span>UTN</span></div>
		</div>
		<br>   
		<div class="login" action="" method="POST">      
            <input type="text" placeholder="Username" name="username"><br>
            <input type="password" placeholder="Password" name="password"><br>
            <input type="submit" value="Login"><br>  
            <p>Crear nueva cuenta <a href="index.php?page=cliente/signup.php" style="color: #FF0000;">aquí</a></p>  
		</div>
    </form>

</body>
</html>