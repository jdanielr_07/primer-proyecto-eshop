<?php

$connection = mysqli_connect('localhost', 'root', 'Logaritmos506', 'primer_proyecto_web1');

$id = $user->getId();

$sql = "SELECT * FROM acciones_cliente WHERE id_usuario = $id";
$result = $connection->query($sql);
$compras = $result->fetch_all();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
</head>
<style>
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: #333;
}

li {
  float: left;
  border-right:1px solid #bbb;
}

li:last-child {
  border-right: none;
}

li a {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

li a:hover:not(.active) {
  background-color: #111;
}

li input {
  display: block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  background-color: purple;
}

li input:hover:not(.active) {
  background-color: #111;
}

.active {
  background-color: purple;
}
#g-table tbody tr > td{
    border: 1px solid rgb(220,220,220);
    height: 30px;
    padding-left: 3px;
}
#g-table{
    padding-left: 40px;
    margin-top: 20px;
}
nav > ul {
  display: flex;
  flex-direction: column;
  align-items: center;
}
</style>
<body style="background-color:gray">
<form action="" method="POST" ectype="multipart/form-data">
    <div id="menu">
        <ul>          
            <li style="float:right"><a href="includes/logout.php">Cerrar sesión</a></li>
            <li style="float:right" class="active"><a href="index_cliente.php?page=cliente/home.php">Volver</a></li>      
        </ul>
    </div>
    <table align="center" class="table table-light"  id="g-table">
    <tr>
        <th>Fecha de compra</th>
        <th>Total de la orden</th>
        <th>Item</th>
        <th>Cantidad</th>
        <th>Descripcion</th>
        <th>Precio</th>
      </tr>
      <tbody>     
          <?php           
              foreach ($compras as $compra) {
                $fecha_compra = $compra[1];
                $total_orden = $compra[2];
                $item = $compra[3];
                $cantidad = $compra[4];
                $descripcion = $compra[5];
                $precio = $compra[6];
                echo "<tr><td>$fecha_compra</td><td>$total_orden</td><td>$item</td><td>$cantidad</td><td>$descripcion</td><td>$precio</td></tr>";
              }
                $sql6 = "SELECT SUM(cantidad) as total FROM acciones_cliente where id_usuario = $id";
                $result6 = mysqli_query($connection,$sql6);
                $fila6 = mysqli_fetch_assoc($result6);
                $cantidad_productos = $fila6['total'];

                $sql7 = "SELECT SUM(total_orden) as total FROM acciones_cliente where id_usuario = $id";
                $result7 = mysqli_query($connection,$sql7);
                $fila7 = mysqli_fetch_assoc($result7);
                $total_orden = $fila7['total'];
                if($cantidad_productos == null){
                    $cantidad_productos = 0;
                }
                if($total_orden == null){
                    $total_orden = 0;
                }
                echo "<table align='center' class='table table-light'  id='g-table'>
                <tbody>
                    <tr>
                    <td><strong>Productos totales: $cantidad_productos</strong></td>
                    <td><strong>Monto total: ₡$total_orden</strong></td></tr>
                </tbody>
              </table>";
          ?>
        </form>   
      </tbody>
    </table>
</body>
</html>